let
  sources = import ./nix/sources.nix;
  pkgs = import sources.nixpkgs {
    overlays = [
      (import (sources.poetry2nix + "/overlay.nix"))
    ];
  };
  pythonEnv = import ./nix/pythonEnv.nix { inherit pkgs; };
in
pkgs.mkShell {
  name = "pb11mpi";
  nativeBuildInputs = with pkgs; [
    cmake
    gcc
    mkl
    ninja
    openmpi
    pythonEnv
  ];
  buildInputs = with pkgs; [
  ];
  hardeningDisable = [ "all" ];
  NINJA_STATUS = "[Built edge %f of %t in %e sec] ";
  MKLROOT = "${pkgs.mkl.out}";
}
