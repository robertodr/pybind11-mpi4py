set(CPACK_PACKAGE_NAME "pb11mpi")
set(CPACK_SOURCE_PACKAGE_FILE_NAME "pb11mpi")
set(CPACK_PACKAGE_VENDOR "Roberto Di Remigio")
set(CPACK_PACKAGE_DESCRIPTION_FILE "${PROJECT_SOURCE_DIR}/README.md")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "An example repository showing how to use MPI with a hybrid C++/Python code")
set(CPACK_RESOURCE_FILE_LICENSE "${PROJECT_SOURCE_DIR}/LICENSE")
set(CPACK_PACKAGE_CHECKSUM SHA256)

set(CPACK_PACKAGE_VERSION_MAJOR "${PROJECT_VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${PROJECT_VERSION_MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH "${PROJECT_VERSION_PATCH}")

list(APPEND _to_ignore
  ${PROJECT_BINARY_DIR}
  "/\.git/"
  "/\.gitlab/"
  "\.gitlab-ci\.yml"
  "\.gitignore"
  "/build*/"
  "/nix/"
  "shell\.nix"
  "poetry\.lock"
  "/\.ccls-cache/"
  "/\.pytest_cache/"
  "__pycache__"
  "\.envrc"
  "[A-Za-z0-9_]\.[ado]$"
  )
set(CPACK_SOURCE_IGNORE_FILES
  ${_to_ignore}
  )
set(CPACK_SOURCE_GENERATOR "TBZ2")
set(CPACK_GENERATOR "TBZ2")

include(CPack)
