#include "hello.hpp"

#include <iostream>

#include <mpi.h>

#ifdef HAS_MKL
#include <mkl_service.h>
#endif

void say_hello(MPI_Comm comm) {
  int size = 0;
  MPI_Comm_size(comm, &size);

  int rank = 0;
  MPI_Comm_rank(comm, &rank);

  std::cout << "Hello from rank " << rank << " of " << size << std::endl;

#ifdef HAS_MKL
  auto buffer = new char[1024];
  mkl_get_version_string(buffer, 1024);
  std::cout << "MKL version: " << buffer << std::endl;
  delete[] buffer;
#endif

  MPI_Finalize();
}
