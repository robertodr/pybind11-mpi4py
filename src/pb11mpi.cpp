#include <mpi.h>
// see here: https://github.com/mpi4py/mpi4py/issues/19#issuecomment-768143143
#ifdef MSMPI_VER
#define PyMPI_HAVE_MPI_Message 1
#endif
#include <mpi4py/mpi4py.h>
#include <pybind11/pybind11.h>

#include "hello.hpp"

namespace py = pybind11;

/*! Return a MPI communicator from mpi4py communicator object. */
MPI_Comm *get_mpi_comm(py::object py_comm) {
  auto comm_ptr = PyMPIComm_Get(py_comm.ptr());

  if (!comm_ptr)
    throw py::error_already_set();

  return comm_ptr;
}

PYBIND11_MODULE(_pb11mpi, m) {

  // initialize mpi4py's C-API
  if (import_mpi4py() < 0) {
    // mpi4py calls the Python C API
    // we let pybind11 give us the detailed traceback
    throw py::error_already_set();
  }

  m.doc() = R"pbdoc(
        Pybind11-mpi4py example plugin
        ------------------------------
        .. currentmodule:: _pb11mpi
        .. autosummary::
           :toctree: _generate
           greetings
    )pbdoc";

  m.def(
      "greetings",
      [](py::object py_comm) {
        auto comm = get_mpi_comm(py_comm);
        say_hello(*comm);
      },
      R"pbdoc(
           Print greetings.
    )pbdoc");
}
